from django.db import models

# Create your models here.
class Redirection(models.Model):
    slug = models.CharField(max_length=100),
    url = models.CharField(max_length=250),
    date = models.DateTimeField('date created'),
    hits = models.IntegerField(default=0)
    