from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings
import short_url
import urllib.request

def tiny_url(url):
    apiurl = "http://tinyurl.com/api-create.php?url="
    tinyurl = urllib.request.urlopen(apiurl + url).read()
    return tinyurl.decode("utf-8")


# Create your views here.
def index(request):
    url = request.GET.get('url')
    print(url)
    short = ""
    if url is not None :
        short = tiny_url(url)
    else :
        url = ""
    return render(request, 'home.html', locals())